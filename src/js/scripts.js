"use strict";

// search bar

$(function () {
    $('#text-search').bind('keyup change', function (ev) {
        // pull in the new value
        var searchTerm = $(this).val();

        // remove any old highlighted terms
        $('body').removeHighlight();

        // disable highlighting if empty
        if (searchTerm) {
            // highlight the new term
            $('body').highlight(searchTerm);
        }
    });
});



// slideshow
var rand = Math.floor(Math.random() * 3);;
$("#slideshow > div:gt(" + `${rand}` + ")").hide();

setInterval(function () {
    $('#slideshow > div:first')
        .fadeOut(1000)
        .next()
        .fadeIn(1000)
        .end()
        .appendTo('#slideshow');
}, 3000);

// read more buttons
$(function () {

    $('button').click(function (event) {
        $(this).parents('.details').find('.read_more').show();
        $(this).hide();
    });

});